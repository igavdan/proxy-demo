'use strict';

// Defines
var stylesSources = "./styles.styl";
var stylesDestination = "./";

// Modules
var gulp = require('gulp'),
  stylus = require('gulp-stylus'),
  watch = require('gulp-watch'),
  plumber = require('gulp-plumber'),
  browserSync = require("browser-sync"),
  browserSyncReload = browserSync.reload,
  browserSyncCreate = browserSync.create();

// Tasks
gulp.task('build-stylus', function () {
  gulp.src(stylesSources)
    .pipe(plumber())
    .pipe(stylus())
    .pipe(gulp.dest(stylesDestination))
    .pipe(browserSyncReload({stream: true}));
});

gulp.task('watch', function () {
  watch([stylesSources], function (event, cb) {
    gulp.start('build-stylus');
  });
});


gulp.task('webserver', function () {
  var localServer = "localhost";
  var localPort = 8080;
  var targerServer = "http://sass-scss.ru/"; // or anything else
  var isProxyServer = true;

  var config = {
    host: localServer,
    port: localPort,
    open: false,
    debug: false,
    stream: true,
    injectChanges: true,
    //rewriteRules: [
    //  {
    //    match: /(assets\/css\/sass.css)/g,
    //    replace: "./styles.css"
    //  }
    //]
  };


  if(!isProxyServer) {
    config.server = {
      baseDir: "./"
    };
  } else {
    config.proxy = {
      target: targerServer,
      ws: true,
      proxyRes: [
        function (res, req) {
          console.log("URL", req.originalUrl);
          if (req.originalUrl === "/assets/css/sass.css") {
            console.log(">>>>>>>>> URL: @TODO: REPLACE THIS CONTENTS");
            //res.write(".....");
            //res.close();
            //console.log("req ===================================", req);
            //console.log("res ===================================", res);
          }
        }
      ]
    };
  }

  // isHTTPS
  if (targerServer.indexOf("https") !== -1) {
    config.https = true;
  }

  browserSync(config);
});

gulp.task('default', ['build-stylus', 'webserver', 'watch']);